#!/bin/sh
docker build -t yourdockerid/image-name:tag ./src
docker run --rm -ti -p 80:80 -e VERSION=0.4 yourdockerid/image-name:tag