# Let's start from basics

Every DevOps need to know basics of Linux and shell scripting. Sometimes you have only simple tools to **Get Things Done**.

Prove to yourself that you deserve to be one of us!

## Your task

Build image serving *index.html* at http://localhost using provided *index_template.html* and tools from base Alpine Linux

After executing

    docker run --rm -ti -p 80:80 -e VERSION=0.1 yourdockerid/image-name:tag

target web page should display something like this:

---

## Hello from Docker!

**DevOps:** your@email.address

Container host: 89a755dfa10b

App version: 0.1

---

## Requirements

- you can't add new packages to base Alpine image,
- you have to convert *index_template.html* to *index.html* using commands provided in base Alpine image,
- you have to serve *index.html* using commands provided in base Alpine image,
- you have to use environment variable VERSION to display version in *index.html*,
- you have to provide your email address as environment variable in Dockerfile (do not hardcode it in html),
- remember to display current HOSTNAME in *index.html*,

## Remarks
- want impress us more? let container log speak in your name - what is runtime environment, what is going on, errors, etc.

